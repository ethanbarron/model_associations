class Employee < ApplicationRecord
  belongs_to :company

  has_many :listing_owners, as: :owner
  has_many :listings, through: :listing_owners
end
