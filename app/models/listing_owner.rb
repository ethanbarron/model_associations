class ListingOwner < ApplicationRecord
  belongs_to :owner, polymorphic: true
  belongs_to :listing
end
