# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

company = Company.create(name: 'Super Company Inc.')

user = User.create(email: 'jim@jimsworld.com')

agent = Agent.create(email: 'bob@bobland.com')

employee = Employee.create(email: 'man@company.com', company: company)

listing_1 = company.listings.create(name: 'Listing #1')
employee.listings << listing_1
employee.save

listing_2 = user.listings.create(name: 'Listing #2')
agent.listings << listing_2
agent.save