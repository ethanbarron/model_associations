class CreateListingOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :listing_owners do |t|
      t.string :owner_type
      t.integer :owner_id
      t.integer :listing_id

      t.timestamps
    end
  end
end
